import {assert} from './ts/assert.ts';
import {SVG_EXT, SVG_RASTERIZE_EXT, StaticHost} from './ts/art.ts';
import {dirName, fsFileExt} from './ts/fs.ts';
import {loadLinesForCommand} from './shell.ts';

import {
  isExpectedImageSuffix,
  Geometry2D,
} from './image.ts';

import {CommandlineError} from './shell.ts';

const IGNORE_LEAVE_RELATIVE: number = -1;
export interface ResizableImage {
  filepath: string;
  geo: Geometry2D;
}

export async function resizeImageWidth(
  fromImg: ResizableImage,
  toPath: string,
  toWidth: number,
): Promise<void> {
  const toImg: ResizableImage = {
    filepath: toPath,
    geo: {
      width: toWidth,
      height: IGNORE_LEAVE_RELATIVE,
    },
  };
  return await resizeImage(fromImg, toImg);
}

async function resizeImage(
  fromImg: ResizableImage,
  toImg: ResizableImage,
) {
  assert(
      toImg.geo.width > 0 || toImg.geo.height > 0,
      `at least one target dimension required for resize`);

  const ext: string = assert(
    fsFileExt(fromImg.filepath),
    `unable to determine source image's file extension: ${fromImg.filepath}`);

  await Deno.mkdir(dirName(toImg.filepath), {recursive: true});

  if (ext === SVG_EXT) {
    return await resizeImageSvg(fromImg, toImg);
  } else {
    return await resizeImageRaster(fromImg, toImg);
  }
}

async function resizeImageSvg(
  fromImg: ResizableImage,
  toImg: ResizableImage,
) {
  let dimensionArgs: Array<string> = [];
  if (toImg.geo.width === IGNORE_LEAVE_RELATIVE) {
    dimensionArgs = ['-h', toImg.geo.height ];
  } else if (toImg.geo.height === IGNORE_LEAVE_RELATIVE) {
    dimensionArgs = ['-w', toImg.geo.width];
  } else {
    dimensionArgs = [
      '-w', toImg.geo.width,
      '-h', toImg.geo.height,
    ];
  }

  assert(
      fsFileExt(toImg.filepath) === SVG_RASTERIZE_EXT,
      `can only resize SVG to raster (expected .${
        SVG_RASTERIZE_EXT} ext) but got filepath "${
        toImg.filepath}"`);

  /**
   * See identify(1) utility from imagemagick.
   *
   * Here's a description of how this command is designed to be used. This
   * `loadLinesForCommand` call corresponds to the rsvg-convert command.:
   *   ```sh
   *   $ identify -verbose orig/path/foo.svg | grep --color=auto geometry
   *      Page geometry: 2488x2064+0+0
   *      # note: 1.205 ratio
   *   $ rsvg-convert -w 600 orig/path/foo.svg -o thumb/path/foo.png
   *   $ identify -verbose thumb/path/foo.png | grep --color=auto geometry
   *      Page geometry: 600x498+0+0
   *      # note: ratio of 1.205 is retained!
   *   ```
   */
  const cliResult: Array<string> = await loadLinesForCommand([
    'rsvg-convert',
    ...dimensionArgs,
    fromImg.filepath,
    '-o',
    toImg.filepath,
  ]);

  assert(
      cliResult.length === 0,
      `unexpected output from rsvg-convert - perhaps it's a warning or error? got: "${
            cliResult}"`);
}

async function resizeImageRaster(
  fromImg: ResizableImage,
  toImg: ResizableImage,
) {
  let dimensionArg: string = '';
  if (toImg.geo.width === IGNORE_LEAVE_RELATIVE) {
    dimensionArg = `x${toImg.geo.height}`;
  } else if (toImg.geo.height === IGNORE_LEAVE_RELATIVE) {
    dimensionArg = `${toImg.geo.width}x`;
  } else {
    dimensionArg = `${toImg.geo.width}x${toImg.geo.height}`;
  }

  /**
   * See identify(1) utility from imagemagick.
   *
   * Here's a description of how this command is designed to be used. This
   * `loadLinesForCommand` call corresponds to the ImageMagick convert(1)
   * command.:
   *   ```sh
   *   $ identify -verbose orig/path/foo.png | grep --color=auto geometry
   *      Page geometry: 2488x2064+0+0
   *      # note: 1.205 ratio
   *   $ convert -resize 600x orig/path/foo.png thumb/path/foo.png
   *   $ identify -verbose thumb/path/foo.png | grep --color=auto geometry
   *      Page geometry: 600x498+0+0
   *      # note: ratio of 1.205 is retained!
   *   ```
   */
  const cliResult: Array<string> = await loadLinesForCommand([
    'convert',
    '-resize',
    dimensionArg,
    fromImg.filepath,
    toImg.filepath,
  ]);

  assert(
      cliResult.length === 0,
      `unexpected output from convert - perhaps it's a warning or error? got: "${
            cliResult}"`);
}

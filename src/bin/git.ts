import {assert} from './ts/assert.ts';

import {loadLinesForCommand} from './shell.ts';

const OPTS: {[Opt: string]: boolean|number} = {
  ParseGitDiffNameOnly: true,
  isDebugging: false,
};
export {OPTS};

export interface GitOrigin {
  originalFilepath: string;
  originatingCommitId: string;
}

export async function gitFindOrigin(filepath: string): GitOrigin {
  /**
   * ```
   *     $ git log --name-only --format='%H' --follow --diff-filter=A --find-renames=0 -- moved-and-modified
   *  1  d87ff93c23dfe68f8ab1c3af7ff980
   *  2
   *  3  foo/bar/original/filepath.txt
   * ```
   */
  const remainingArg: string = OPTS.ParseGitDiffNameOnly ? '--name-only' : '--patch';
  const stdout: Array<string> = await loadLinesForCommand([
    'git',
    'log',
    '--format=%H',
    '--follow',
    '--diff-filter=A',
    '--find-renames=0',
    remainingArg,
    '--',
    filepath,
  ]);

  assert(
      stdout.length > 2,
      `got ${stdout.length} lines of output from git but expected more, for file "${filepath}"`);

  const commitHash: string = String(stdout[0] || '').trim();
  // dbg(`commitHash='${commitHash}'`);
  assert(commitHash.length, `first line should have had a commit hash, but got no output`);

  // TODO figure out how to implement this without this hacky scraping; eg: use
  // --format value or something?
  const scrapedFilePath = scrapeOriginalFilename(filepath, stdout);
  assert(scrapedFilePath.length, `found empty _to_filename for file: "${filepath}"`);

  return {
    originalFilepath: scrapedFilePath,
    originatingCommitId: commitHash,
  }
}

/**
 * Scrapes very particular `git log` output to try to determine the original
 * name of a file.
 *
 * ## Expected `git log` output
 *
 * `gitStdout` can have two forms: on for text fil contents that git is willing
 * to print to stdout, and another for "binary" files.
 *
 * The former output would look like this:
 *        ```
 *        $ git log --patch --format='%H' --follow --diff-filter=A --find-renames=0 -- moved-and-modified
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  diff --git a/2023-01-28T15:29:28-06:00 b/2023-01-28T15:29:28-06:00
 *     4  new file mode 100644
 *     5  index 0000000..b5dac5f
 *     6  --- /dev/null
 *     7  +++ b/2023-01-28T15:29:28-06:00
 *     8  @@ -0,0 +1 @@
 *     9  +Foo Bar Baz
 *        ```
 *
 * The latter output would look like this:
 *        ```
 *        $ git log --patch --format='%H' --follow --diff-filter=A --find-renames=0 -- file-never-moved
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  diff --git a/file-never-moved b/file-never-moved
 *     4  new file mode 100644
 *     5  index 0000000..58e8cca
 *     6  Binary files /dev/null and b/file-never-moved differ
 *        ```
 *
 * This is all true UNLESS OPTS.ParseGitDiffNameOnly is enabled in which cas a
 * different command is used with _seemingly_ more reliable output:
 *        ```
 *        $ git log --name-only --format='%H' --follow --diff-filter=A --find-renames=0 -- original/file/path.txt
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  original/file/path.txt
 *        ```
 */
function scrapeOriginalFilename(currentFilePath: string, gitStdout: Array<string>): string {
  function errMsg(baseMsg: string): string {
    const addition = !OPTS.isDebugging ?
        '' :
        `[extra -debug output]: OPTS.ParseGitDiffNameOnly=${OPTS.ParseGitDiffNameOnly}; git stdout lines were:\n"""\n${gitStdout.join('\n')}\n"""\n`;
    return `fragile scraper failure: ${
        baseMsg} for file: "${
        currentFilePath}"\n\tconsider trying bin/debug-git-origin.sh on this file\n${addition}`;
  };
  const bSlashRegExp = /b\//;
  if (OPTS.ParseGitDiffNameOnly) {
    assert(
        gitStdout.length === 3,
        errMsg(`expected exactly 3 lines`));
    const lastLine: string = String(gitStdout[2] || '').trim();
    assert(lastLine.length, errMsg(`expected non-empty last-line`));
    return lastLine;
  }

  const origFileLineNum: number = gitStdout.findIndex(line => line === '--- /dev/null');
  if (origFileLineNum === -1) { // this was NOT a file rename
    assert(
        gitStdout.length >= 6,
        errMsg(`short on diff line-count (${gitStdout.length}) for "binary" file diff`));
    const lastLine: string = String(gitStdout[gitStdout.length-1] || '').trim();
    assert(
        lastLine.length,
        errMsg(`unexpected empty final line of "binary diff" output`));

    assert(
        lastLine.startsWith('Binary files ') && lastLine.endsWith(' differ'),
        errMsg(`unrecognized overall content (expected "binary diff" output)`));

    const scrapedOriginalFilePath: string = lastLine.
        replace(/^Binary files .* and /, '').
        replace(/ differ$/, '').
        replace(bSlashRegExp, '').
        trim();
    // TODO consider running `git show COMMITID scrapedOriginalFilePath` as a
    // sanity-check that we scraped a filepath and not some other random strings
    // out of the diff output.
    assert(
        scrapedOriginalFilePath.length,
        errMsg(`turned up empty results`));
    return scrapedOriginalFilePath;
  } else {
    assert(
        gitStdout.length > 7,
        errMsg(`short on diff line-count (${gitStdout.length}) for "text" diff`));

    assert(
        origFileLineNum < gitStdout.length - 2,
        errMsg(`found /dev/null on last line of output but expected _to_ filename`));
    /**
     * from:
     *   """
     *   +++ b/some/file/path.txt
     *   """
     * to:
     *   """
     *   some/file/path.txt
     *   """
     */
    const scrapedFilePath: string =
        String(gitStdout[origFileLineNum+1] || '').
        replace(/^\+.. /, '').
        replace(bSlashRegExp, '').
        trim();
    assert(
        scrapedFilePath.length,
        errMsg(`expected non-empty _to_ filename line`));
    return scrapedFilePath;
  }
}

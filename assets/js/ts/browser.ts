import {assert} from './assert.ts';

type StrStrObj = {[attr: string]: string};

export class Dom {
  constructor(public doc: Document) {}

  ready(): Promise<void> {
    if (this.doc.readyState === 'complete') {
      return Promise.resolve();
    }
    return new Promise((resolve, reject) => {
      this.doc.addEventListener('readystatechange', () => {
        if (this.doc.readyState === 'complete') {
          resolve();
        }
      });
    });
  }

  queryOne(query: string): Element {
    return assert(
      this.doc.querySelector(query),
      `DOM query failed; expected to find: "${query}"`);
  }

  appendNewEl(
    parent: Element,
    nodeName: string,
    attrs: StrStrObj = {},
  ): Element {
    const el = this.buildEl(nodeName, attrs);
    parent.appendChild(el);
    return el;
  }

  buildEl(
    nodeName: string,
    attrs: StrStrObj = {},
  ): Element {
    const el = this.doc.createElement(nodeName);
    Dom.setAttrs(el, attrs);
    return el;
  }

  static setAttrs(e: Element, attrs: StrStrObj = {}) {
    for (let attr in attrs) {
      e.setAttribute(attr, attrs[attr]);
    }
    return e;
  }
}

export function fetchAsJson<T>(jsonPath: string): Promise<T> {
  return fetch(jsonPath).then(resp => resp.json());
}

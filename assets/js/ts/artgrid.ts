import {assert} from './assert.ts';
import {shuffleArray} from './common.ts';
import {Dom} from './browser.ts';
import {TargetSmallWidthPx, StaticHost} from './art.ts';
import {ArtworkIndex, Artwork, ArtworkDir} from './artapi.ts';

/**
 * @fileoverview This file conforms to the template expectations of
 * app/layouts/jslib.html; namely: this file expects jslibJsArgs to be a window
 * global containing the Google Drive HOST id of a public folder under which it
 * can find:
 *   1) an index.json file at the root listing the folder's contents
 *   2) art work to be displayed in this template.
 */

///////////////////////////////////////////////////////////////////////////////

export class ArtGrid {
  private gridEl: Element;
  private index_?: Array<Artwork>;
  private randomizedWorksToLoad: Array<Artwork> = [];
  private loading: Promise<void>;
  private containerEl: Element;

  private static readonly FlagEnableCaption: boolean = false;

  /** Standard size of sections to add to grid. */
  private static readonly GRID_ADDITION: number = 8;

  private constructor(
    private staticHost: StaticHost,
    private dom: Dom,
    root: Element,
  ) {
    this.containerEl = ArtGrid.injectContainer(dom, root);
    this.gridEl = dom.buildEl('ul');
    this.buildGrid();

    // Fetch artwork index
    this.loading = staticHost.
        fetchIndex().
        then((artworkIndex: ArtworkIndex): void => {
          this.index_ = artworkIndex;
          this.randomizedWorksToLoad = Array.
                from(this.index_.contents).
                filter(art => this.isGoodAdd(art.filename));
          shuffleArray(this.randomizedWorksToLoad);
        });
  }

  /** Builds artwork grid and attaches to DOM as soon as browser's ready. . */
  static init(w: Window): Promise<ArtGrid> {
    const dom = new Dom(w.document);
    dom.ready().then(() => {
      let rootNode: Element = dom.queryOne('section#matter');

      let staticHost = new StaticHost(StaticHost.defaultRootPrefix /*TODO should be zero args*/);
      let grid = new ArtGrid(staticHost, dom, rootNode);

      grid.ready().then(
          () => assert(grid.addRandomArt(), 'already out of artwork'));
      return artwork;
    });
  }

  private get index() {
    return assert(this.index_, 'index referred to before it was loaded');
  }

  private buildGrid() {
    Dom.setAttrs(this.gridEl, {'class': 'grid'});
    this.containerEl.appendChild(this.gridEl);

    let buttonEl: Element = this.dom.buildEl('button');
    buttonEl.textContent = 'Load more';
    buttonEl.addEventListener('click', () => {
      const moreWorkPending: boolean = this.addRandomArt();
      if (moreWorkPending) return;
      buttonEl.remove();
    });
    this.containerEl.appendChild(buttonEl);
  }

  /** Promise be ready to render artwork. */
  ready(): Promise<void> {
    // TODO no way to void() the return of Promise.all? (eg a
    // Promise.all.void()?)
    return this.index_ ? Promise.resolve() : Promise.all([this.loading]).then();
  }

  /**
   * @param {!Document} doc
   * @param {!Element} rootNode
   * @return {!Element} container
   * @private
   */
  private static injectContainer(dom: Dom, rootNode: Element) {
    var containerEl = dom.buildEl('div', {'id': 'artwork'});
    rootNode.appendChild(containerEl);
    return containerEl;
  }

  /** Renders a visual grid of artwork on the page. */
  addToGrid(artwork: Array<Artwork>) {
    artwork.forEach((work: Artwork) => {
      if (!this.isGoodAdd(work.filename)) {
        return;
      }

      this.addArtToGrid(work);
    });
  }

  private isGoodAdd(artwork: string): boolean {
    // TODO(zacsh): deal with this when you break artwork into "vector" vs
    // "raster" vs "doodle"
    return Boolean(
        // TODO figure out why this was added and/or delete this condition
        typeof artwork != 'object' &&

        // TODO delete this (the index builder prevents this from showing up now
        artwork != 'copying');
  }

  /**
   * Appends a thumbnail for `fileName` to ArtGrid grid.
   *
   * @return newly appended thumbnail for {@code fileName}
   */
  addArtToGrid(artwork: Artwork): Element {
    let fileUrl: string = '/' + artwork.filename;

    let thumbNailEl: Element = this.dom.appendNewEl(
        this.gridEl,
        'li',
        {'class': 'thumbnail'});

    let anchorEl: Element = this.dom.appendNewEl(thumbNailEl, 'a', {
      'target': '_blank',
      'href': fileUrl,
    });

    let imgEl: Element = this.dom.appendNewEl(
        anchorEl, 'img', {'src': this.staticHost.toThumbPath(fileUrl)});
    // TODO(zacsh) remove this hard-coded performance net for the browser, once
    // i am serving multiple sizes
    imgEl.setAttribute('width', TargetSmallWidthPx+'px');

    if (ArtGrid.FlagEnableCaption) {
      let captionEl: Element = this.dom.appendNewEl(
          anchorEl,
          'span',
          {'class': 'caption'});
      captionEl.textContent = artwork.filename;
    }

    return thumbNailEl;
  }

  /**
   * Loads random small subset of artwork into the grid.
   *
   * @return whether there's still more artwork waiting to be loaded.
   */
  addRandomArt(): boolean {
    const nextRandomWorks =
      this.randomizedWorksToLoad.splice(0, ArtGrid.GRID_ADDITION);
    this.addToGrid(nextRandomWorks);

    return !!this.randomizedWorksToLoad.length;
  }
}
